def newtons(f, df, initial_guess, threshold) : 
    print('{:>20}|    {:>20}|    {:>20}'.format('n','x','dx'))
    print('-'*70)
    x0 = 0
    x1 = initial_guess
    n = 0
    print('{:20}|    {:20}|    {:20}'.format(n, x1, abs(x1 - x0)))     
    while abs(x1 - x0) > threshold : 
        x0 = x1
        fx = f(x0)
        dfx = df(x0)
        x1 = x0 - fx/dfx
        
        n += 1
        print('{:20}|    {:20}|    {:20}'.format(n, x1, abs(x1 - x0)))       
    return x1