from newton import newtons

def f(x) : 
    return 2*x**7 + 4*x**5 - 2*x**3 + 3*x + 1.
    
def df(x) : 
    return 14*x**6 + 20*x**4 - 6*x**2 + 3.
    
if __name__ == '__main__' : 
    newtons(f, df, 10., 1.e-8)
