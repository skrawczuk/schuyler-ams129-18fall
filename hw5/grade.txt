7/10

There are a couple issues with your code:

1. The derivative function df was not correct. I fixed it for you.
2. The format of the output did not match what was requested.
