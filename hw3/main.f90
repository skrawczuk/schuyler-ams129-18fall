program main
    use basel, only: calc_basel
    use param
    implicit none
    
    real(kind=8) :: threshold, solution
    
    print *, 'enter the desired threshold as a real number:'
    read(*,*) threshold
    
    call calc_basel(threshold, solution)

    print *, solution
    
end program main
