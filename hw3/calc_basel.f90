module basel
            
contains    
            
    subroutine calc_basel(threshold, solution)
        use param, only: pi
        implicit none
        
        real(kind=8), intent(in)  :: threshold
        real(kind=8), intent(out) :: solution
        real(kind=8) :: error, actual_solution
        integer :: i = 1
        
        solution = 0
        actual_solution = pi**2 / 6.d0
        error = 10
        
        open(unit=1, file='results.txt') 
        
        do while (error > threshold) 
            solution = solution + 1./real(i)**2
            error = actual_solution - solution
            
            if (mod(i,1000) == 0) then 
                write(1, *) 'n =',i, error
            end if
            
            i = i+1
        end do
        
        write(1, *) 'total cycles =',i, 'solution = ', solution, 'error =',error
        close(unit=1)
    
    end subroutine calc_basel
    
end module basel

