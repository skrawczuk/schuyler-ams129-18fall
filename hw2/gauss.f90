program gauss
    
    implicit none
    
    real(kind=8), dimension(3,3) :: A
    real(kind=8), dimension(3) :: b
    real :: a_fraction
    
    integer :: i, j
    
    ! initialize matrix A and vector b
    A(1,:) = (/2., 3., -1./)
    A(2,:) = (/4., 7., 1./)
    A(3,:) = (/7., 10., -4./)
    
    b = (/1., 3., 4./)
    
    ! print augmented matrix
    do i = 1, 3           ! i is row
        print *, A(i,:), "|", b(i)
    end do
    
    print *, ""    ! print a blank line
    print *, "Gaussian elimination"
    ! gaussian elimination
        do j = 1, 2             ! j is column
            do i = j+1, 3       ! i is row
                a_fraction = A(i,j)/A(j,j) ! temp stored so no change for b
                A(i,:) = A(i,:) - A(j,:) * a_fraction
                b(i)   = b(i)   - b(j)   * a_fraction
            end do
        end do
        
    ! print augmented matrix again
    ! this should be an echelon form (or triangular form)
    print *, "***********************"
    do i = 1, 3
        print *, A(i,:), "|", b(i)
    end do
    
    print *, ""    ! print a blank line
    print *, "back subs......"
    
    
    ! doing back substitution
    do j = 3, 2, -1           ! j is column
        do i = j-1, 1, -1     ! i is row
            a_fraction = A(i,j)/A(j,j)
            A(i,:) = A(i,:) - A(j,:) * a_fraction
            b(i)   = b(i)   - b(j)   * a_fraction
        end do
    end do
    
    ! print the results
    print *, "***********************"
    do i = 1, 3
        print *, A(i,:), "|", b(i)
    end do
    
    print *, "The solutions are:"
    
    do i = 1,3
        print *, b(i) / A(i,i)
    end do

end program gauss
