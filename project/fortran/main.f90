program main
    use euler
    implicit none
    
    real(8)      :: t_0, t_f, y_0
    integer      :: N, i
    character(len=13)    :: file_name, file_num
    
    y_0 = -2
    t_0 = 0
    t_f = 10
    
    do i=1,4
        N = 2**(i+2)
        write(file_num, '(i0)') N 
        file_name = 'output_' // trim(adjustl(file_num)) // '.txt'
        
        call eulers_method(t_0, y_0, t_f, N, file_name) 
    end do
    
end program main