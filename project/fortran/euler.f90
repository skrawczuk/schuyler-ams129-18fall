module euler
implicit none

contains 

    real function dydt(t,y)
        real(8) :: t, y
        
        dydt = 2*t / (y*(1+t**2))
    end function dydt
    

    subroutine eulers_method(t_0, y_0, t_f, N, file_name)
        real(8), intent(in)           :: t_0, t_f, y_0
        character(len=13), intent(in) :: file_name
        real(8)                       :: t_n, y_n, h
        integer                       :: i, N
        
        h = (t_f - t_0) / (N-1.)
        y_n = y_0
        t_n = t_0
        
        open(unit=1, file=file_name)
        write(1,*) t_n, y_n
        
        do i=1,N-1 
            t_n = t_0 + i*h
            y_n = y_n + h*dydt(t_n, y_n)
            
            write(1,*) t_n, y_n
        end do
        
        close(unit=1)
    end subroutine eulers_method

end module euler