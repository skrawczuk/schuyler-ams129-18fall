import os
import numpy as np
import matplotlib.pyplot as plt

# changing directory from python to fortran
cd = os.getcwd()
cd = '/'.join(cd.split('/')[:-1])
os.chdir(cd+'/fortran')

# compiling fortran
os.system('make') 


def f(t) : 
    '''real solution to equation'''
    return -np.sqrt(2*np.log(t**2+1) + 4)

# plotting each set 
for fname in ['output_{}.txt'.format(i) for i in [8,16,32,64]] : 

    output = np.loadtxt(fname)
    t = output[:,0]
    y_num = output[:,1]
    
    y_actual = f(t)
    
    error = np.sum(np.abs(y_actual - y_num))
    
    plt.figure()
    plt.plot(t, y_num, '--or')
    plt.plot(t, y_actual, '-b')
    plt.title(error)
    plt.xlabel('t axis')
    plt.ylabel('y axis')
    plt.grid(1)
    plt.savefig(cd+'/python/result'+fname[6:-3] + 'png')
plt.show()