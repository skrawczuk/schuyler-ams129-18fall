def right_justify(s) : 
    number_of_spaces = 70-len(s)
    spaces = number_of_spaces * ' '
    new_string = spaces + s
    print(new_string)
    
def count_char(s, c) : 
    c_count = 0
    for char in s.lower() : 
        if char == c.lower() : 
            c_count += 1 
    print('Total number of {} in the given string: {}'.format(c, c_count))
    
def cumulative_sum(l) : 
    sum_list = [sum(l[:i+1]) for i in range(len(l))]
    return sum_list
    
def check_palindrome(s) : 
    s_reverse = [s[~i] for i in range(len(s))]
    if list(s) == s_reverse : 
        is_palindrome = True
    else : 
        is_palindrome = False
    return is_palindrome
    
if __name__ == '__main__' : 
    right_justify('Schuyler Krawczuk')   

    lorem = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla iaculis tellus id mollis scelerisque. In consequat nec tellus lacinia iaculis. Mauris auctor volutpat aliquam. Nulla dignissim arcu placerat tellus pretium, vel venenatis sem porta. Donec interdum tincidunt mi, et convallis velit aliquet eget. Nam id rutrum felis. Fusce vel fermentum justo. Pellentesque faucibus orci at velit vehicula dignissim. Duis faucibus dapibus malesuada. Pellentesque iaculis tristique vestibulum. Sed egestas nisl non augue imperdiet mattis. Nunc vitae purus lectus. Vestibulum mi turpis, volutpat vel odio quis, hendrerit suscipit ligula. Nunc in massa diam. Suspendisse aliquam quam et ex egestas vestibulum vitae id libero. Cras molestie consectetur condimentum.'
    count_char(lorem, 't')
    
    result = cumulative_sum([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
    print(result)
    
    words_list = ['noon', 'madam', 'ams129', 'redivider', 'numpy', 'bob', 'racecar', 'youngjun']
    for word in words_list:
        if(check_palindrome(word)):
            print(word, 'is palindrome!')