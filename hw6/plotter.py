import numpy as np
import matplotlib.pyplot as plt
import string


def count_char(s, c) : 
    c_count = 0
    for char in s.lower() : 
        if char == c.lower() : 
            c_count += 1 
    return c_count

def character_count(file_path) : 
    # reading in file as one big string
    with open(file_path) as f : 
        all_chars = f.read().replace('\n', '')

    # counting characters & putting in dictionary 
    count_dict = {}
    for char in string.ascii_lowercase :
        count = count_char(all_chars, char)
        count_dict[char] = count
    return count_dict


file_path = './words.txt'

count_dict = character_count(file_path) 
keys = count_dict.keys()                
values = count_dict.values()            
                                        
# sorting values 
keys, values = zip(*sorted(zip(keys, values)))
values = np.array(values)

# plotting 
x = range(len(keys))
plt.bar(x, values)
plt.plot(x, values, 'ro-')
plt.xlabel('Character')
plt.ylabel('Frequency')
plt.xticks(x, keys)
plt.grid()
plt.savefig('result.png')
plt.show()